<?php

    require_once __DIR__ . '/LiqPay.php';

    use LiqPay; 


    $amount = isset($_POST['amount'])?intval($_POST['amount']):50;

    $public_key  = 'sss';
    $private_key = 'ss';
    $liqpay      = new LiqPay($public_key, $private_key);
    $html        = $liqpay->cnb_form([
        'action'      => 'pay',
        'amount'      => strval($amount),
        'currency'    => 'UAH',
        'description' => 'description text',
        'order_id'    => 'order_id_1',
        'version'     => '3',
    ]);

?>

<style>
.borderpr, .borderpr:focus {
    border: 1px #3c8891 solid;
    background: #3c8891;
    color: #fff;
}

.btn_donate_val{
    border-top-left-radius: 0 !important;
    border-radius: 19px;
}

.btn_donate_pay{
    border-top-left-radius: 0 !important;
    border-radius: 19px;
}

.sizeh1{
    font-size: 2.5em;
}

</style>
<div class="container">
    <div class="row">



        <div class="col-sm">

            <div class="badge bg-info text-white h1 sizeh1" >
                Хочу помочь
            </div>
            <br><br>
            <div class="row">
                <div class="col-sm">
                    <button type="button"
                            class="btn btn-lg btn-primary btn_donate_val"
                            data-value="50">50 грн</button>
                </div>
                <div class="col-sm">
                    <button type="button"
                            class="btn btn-lg btn-light btn_donate_val"
                            data-value="100">100 грн</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm">
                    <button type="button"
                            class="btn btn-lg btn-light btn_donate_val"
                            data-value="200">200 грн</button>
                </div>
                <div class="col-sm">
                    <button type="button"
                            class="btn btn-lg btn-light btn_donate_val"
                            data-value="500">500 грн</button>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm">
                    <button type="button"
                            class="btn btn-lg btn-light btn_donate_val"
                            data-value="1000">1000 грн</button>
                </div>
                <div class="col-sm">
                    <input type="number"
                           class="form-control form-control-lg"
                           placeholder="Другая сумма"
                           id="edit_donate_val">
                </div>
            </div>
            <br><br>

            <input type="hidden"
                   id="donate_value"
                   value="50">

            <div id="liqpay_widget_inner">
                <div>
                    <?=$html;?>
                    <div style="display:none;">
                    Отладка: сумма оплаты <?=$amount?> грн.
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm">
            <div>
                <div class="sppb-addon-content">Перевод
                    можно осуществить на карту приват банка <br><strong>5169 3305 1649 9628</strong> - Сердиченко Петр
                    Владимирович. <br>
                    Назначение платежа: "Благодійні внески, Без НДС"
                </div>
            </div>
            <br>
            <div>
                <div class="sppb-addon sppb-addon-text-block  ">
                    <div class="sppb-addon-content"> Перевод
                        можно осуществить в любом банковском отделении используя следующие реквизиты:<br> <span
                              style="color: #0fa89d;"><strong>&nbsp; &nbsp; &nbsp; Получатель:</strong></span>
                        <strong>МIЖНАРОДНА ДОПОМОГА ДОБРИЙ САМАРЯНИН, БФ</strong><br> <strong><span
                                  style="color: #0fa89d;">&nbsp; &nbsp; &nbsp; Код ЕДРПОУ:</span> 25883822</strong><br>
                        <strong><span style="color: #0fa89d;">&nbsp; &nbsp; &nbsp; МФО: </span>328704</strong><br>
                        <strong><span style="color: #0fa89d;">&nbsp; &nbsp; &nbsp; Расчетный счет:
                            </span>26002054213184</strong><br> <strong><span style="color: #0fa89d;">&nbsp; &nbsp;
                                &nbsp; Назначение платежа:</span> "Благодійні внески, Без НДС"</strong>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>





<script>
jQuery(function() {

    jQuery(".btn_donate_val").click(function() {
        jQuery(".btn_donate_val").removeClass('btn-primary').removeClass('btn-light').addClass(
            'btn-light');
        jQuery(this).removeClass('btn-light').addClass('btn-primary');
        jQuery("#donate_value").val(jQuery(this).data('value'))
        jQuery("#donate_value").change();
        jQuery("#edit_donate_val").val('');
        jQuery("#edit_donate_val").removeClass('borderpr');
    });


    jQuery("#edit_donate_val").keyup(function() {
        var val = Number(jQuery(this).val());

        if (val) {
            jQuery(".btn_donate_val").removeClass('btn-primary').addClass('btn-light');
            jQuery("#donate_value").val(val);
            jQuery(this).addClass('borderpr');
            jQuery("#donate_value").change();
        }
    });

    jQuery("#donate_value").change(function() {
        jQuery("#liqpay_widget_inner").load("/ru/dopomogty #liqpay_widget_inner", {
            amount: jQuery(this).val()
        });
    });
});
</script>
