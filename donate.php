<?php


defined('_JEXEC') or die;


/**
 * 
 *
 * @since  1.5
 */
class PlgContentDonate extends JPlugin
{
    /**
     * Plugin that cloaks all emails in content from spambots via Javascript.
     *
     * @param  string       $context The context of the content being passed to the plugin.
     * @param  mixed        &$row    An object with a "text" property or the string to be cloaked.
     * @param  mixed        &$params Additional parameters. See {@see PlgContentEmailcloak()}.
     * @param  integer      $page    Optional page number. Unused. Defaults to zero.
     * @return boolean    True on success.
     */
    public function onContentPrepare(
         $context,
        &$row,
        &$params,
         $page = 0
    )
    {
        // Don't run this plugin when the content is being indexed
        if ('com_finder.indexer' === $context)
        {
            return true;
        }

        if (is_object($row))
        {
            return $this->_cloak($row->text, $params);
        }

        return $this->_cloak($row, $params);
    }

    /**
     * Заменяет DIV на виджет
     */
    protected function _cloak(
        &$text,
        &$params
    )
    {

		$re = '/<div([\s]*)id=([\"\'])liqpay_donat([\"\'])([\s]*)>([^<]*)<\/div>/imXsU';
		$rpl = 'liqpay_donat_content_1';
		

		
        if (preg_match($re, $text))
        {


            

			$rpl = $this->getWidget($params);
            $text = preg_replace($re, $rpl, $text);

            return true;
		}
		
		



        return true;
	}
	

    protected function getWidget()
    {
        $public_key = $this->params->def('public_key', '');
        $private_key = $this->params->def('private_key', '');
		ob_start();
		include __DIR__.'/widget.php';
		$widget = ob_get_contents();
		ob_clean();
		return $widget;
	}
}
